// import 'package:bottom_navigation/accountScreen.dart';
// import 'package:bottom_navigation/favoriteScreen.dart';
// import 'package:bottom_navigation/homeScreen.dart';
// import 'package:bottom_navigation/reviewScreen.dart';
// import 'package:flutter/material.dart';
// import 'package:rounded_tabbar_widget/rounded_tabbar_widget.dart';
//
// void main() {
//   runApp(MaterialApp(
//     debugShowCheckedModeBanner: false,
//     home: RoundedTabbarWidget(
//       itemNormalColor: Colors.lightBlue[300],
//       itemSelectedColor: Colors.lightBlue[900],
//       tabBarBackgroundColor: Colors.lightBlue[100],
//       tabIcons: [
//         Icons.home,
//         Icons.favorite,
//         Icons.chat,
//         // Icons.person,
//       ],
//       pages: [
//         HomeScreen(title: "Home"),
//         FavoriteScreen(title: "Favorite"),
//         ReviewScreen(title: "Review"),
//        // AccountScreen(title: "Account"),
//       ],
//       selectedIndex: 0,
//       onTabItemIndexChanged: (int index) {
//         print('Index: $index');
//       },
//     ),
//   ));
// }





import 'package:bottom_navigation/customIndicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.black.withOpacity(1),
        statusBarIconBrightness: Brightness.light));
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData.light(),
      darkTheme: ThemeData.light(),
      home: Home(),
    );
  }
}
//
// class MyHomePage extends StatefulWidget {
//
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }
//
// class _MyHomePageState extends State<MyHomePage>
//     with SingleTickerProviderStateMixin {
//   late int currentPage;
//   Color currentColor = Colors.deepPurple;
//   Color inactiveColor = Colors.black;
//   late PageController tabBarController;
//   List<Tabs> tabs = [];
//
//   @override
//   void initState() {
//     super.initState();
//     currentPage = 0;
//     tabs.add(Tabs(
//       Icons.home,
//       "Home",
//       Colors.deepPurple,
//       getGradient(Colors.deepPurple),
//     ));
//     tabs.add(
//         Tabs(Icons.search, "Search", Colors.pink, getGradient(Colors.pink)));
//     tabs.add(
//         Tabs(Icons.alarm, "Alarm", Colors.amber, getGradient(Colors.amber)));
//     tabs.add(Tabs(
//         Icons.settings, "Settings", Colors.teal, getGradient(Colors.teal)));
//     tabBarController = new PageController(initialPage: 0);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     Widget tabView({required int destinationIndex}) {
//       return Container(
//           decoration: BoxDecoration(color: tabs[currentPage].color),
//           child: InkWell(
//               child: Center(
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: <Widget>[
//                       Text(
//                         tabs[currentPage].title,
//                         textAlign: TextAlign.center,
//                         style: TextStyle(
//                             color: Colors.white,
//                             fontSize: 20.0,
//                             fontWeight: FontWeight.w700),
//                       ),
//                       SizedBox(height: 4),
//                       Text(
//                         "Click here to Change the tab To " +
//                             tabs[destinationIndex].title,
//                         textAlign: TextAlign.center,
//                         style: TextStyle(color: Colors.white, fontSize: 14.0),
//                       ),
//                     ],
//                   )),
//               onTap: () {
//                 setState(() {
//                   currentPage = destinationIndex;
//                   tabBarController.jumpToPage(currentPage);
//                 });
//               }));
//     }
//
//     return Scaffold(
//       body: PageView(
//           controller: tabBarController,
//           physics: NeverScrollableScrollPhysics(),
//           children: <Widget>[
//             tabView(destinationIndex: 3),
//             tabView(destinationIndex: 0),
//             tabView(destinationIndex: 1),
//             tabView(destinationIndex: 2)
//           ]),
//       drawer: new Container(
//           width: 250.0,
//           margin: EdgeInsets.only(bottom: 60.0),
//           color: Colors.blue,
//           child: ListView(
//             children: <Widget>[Text("Hello"), Text("World")],
//           )),
//       endDrawer: new Container(
//           width: 250.0,
//           margin: EdgeInsets.only(bottom: 60.0),
//           color: Colors.blue,
//           child: ListView(
//             children: <Widget>[Text("Hello"), Text("World")],
//           )),
//       bottomNavigationBar: CubertoBottomBar(
//         key: Key("BottomBar"),
//         inactiveIconColor: inactiveColor,
//         tabStyle: CubertoTabStyle.STYLE_FADED_BACKGROUND,
//         selectedTab: currentPage,
//         tabs: tabs
//             .map((value) => TabData(
//             key: Key(value.title),
//             iconData: value.icon,
//             title: value.title,
//             tabColor: value.color,
//             tabGradient: value.gradient))
//             .toList(),
//         onTabChangedListener: (position, title, color) {
//           setState(() {
//             currentPage = position;
//             tabBarController.jumpToPage(position);
//           });
//         },
//       ),
//     );
//   }
//
//   @override
//   void dispose() {
//     tabBarController.dispose();
//     super.dispose();
//   }
// }
//
// class Tabs {
//   final IconData icon;
//   final String title;
//   final Color color;
//   final Gradient gradient;
//
//   Tabs(this.icon, this.title, this.color, this.gradient);
// }
//
// getGradient(Color color) {
//   return LinearGradient(
//       colors: [color.withOpacity(0.5), color.withOpacity(0.1)],
//       stops: [0.0, 0.7]);
// }






class Home extends StatefulWidget {


  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin{
  List<Widget> _widgetOptions = <Widget>[
    Page(),
    Page2(),
    Page3(),
  ];
  int _currentSelected = 0;
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  Widget _onItemTapped(int index) {
    // index == 3
    //     ? _drawerKey.currentState?.openDrawer()
    //     : setState(() {
    //   _currentSelected = index;
    // });

    if(index == 3){
      slideSheet();
    } else{
      setState(() {
        _currentSelected = index;
      });
    }
  }

   AnimationController controller;
   Animation<Offset> offset;

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(seconds: 1));

    offset = Tween<Offset>(begin: Offset(0.0, 40.0), end: Offset(0.0, 4.0))
        .animate(controller);
  }



  void slideSheet() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            color: Color(0xFF737373),
            child: Container(
              height: 180,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                color: Colors.white,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text('Row1')
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text('Row2')
                    ],
                  )


                ],
              ),
            ),
          );
        });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xE2FDFDFD),
      key: _drawerKey,
      body: Stack(
        children: [
          Column(
            children: [
              Expanded(child: Container(child: _widgetOptions.elementAt(_currentSelected))),
              InkWell(child: Text("Hello world"),

                onTap: () {
                  // switch (controller.status) {
                  //   case AnimationStatus.completed:
                  //     controller.reverse();
                  //     break;
                  //   case AnimationStatus.dismissed:
                  //     controller.forward();
                  //     break;
                  // }

                  slideSheet();

                }

              )
            ],
          ),
         // getSlidingUp(),

          Align(
            alignment: Alignment.bottomCenter,
            child: SlideTransition(
              position: offset,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Card(

                  color: Colors.red,
                  child: Container(
                    height: 350,
                  ),
                ),
              ),
            ),
          )

        ],
      ),
      drawer: Drawer(),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Card(

          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0),
              topRight: Radius.circular(15.0),
              bottomLeft: Radius.circular(15.0),
              bottomRight: Radius.circular(15.0),
            ),
            child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              onTap: _onItemTapped,
              currentIndex: _currentSelected,
              showUnselectedLabels: true,
              unselectedItemColor: Colors.grey[800],
              selectedItemColor: Color.fromRGBO(10, 135, 255, 1),
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  label: 'Page 1',
                  icon: Icon(Icons.access_alarm),
                ),
                BottomNavigationBarItem(
                  label: 'Page 2',
                  icon: Icon(Icons.accessible),
                ),
                BottomNavigationBarItem(
                  label: 'Page 3',
                  icon: Icon(Icons.adb),
                ),
                BottomNavigationBarItem(
                  label: 'Drawer',
                  icon: Icon(Icons.more_vert),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Page extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Center(child: Container(child: Text("Page 1"),));
  }
}

class Page2 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Center(child: Container(child: Text("Page 2"),));
  }
}

class Page3 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Center(child: Container(child: Text("Page 3"),));
  }
}

Widget _floatingPanel(){
  return Container(
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(24.0)),
        boxShadow: [
          BoxShadow(
            blurRadius: 20.0,
            color: Colors.grey,
          ),
        ]
    ),
    margin: const EdgeInsets.all(24.0),
    child: Center(
      child: Text("This is the SlidingUpPanel when open"),
    ),
  );
}


Widget getSlidingUp() {
  return SlidingUpPanel(
    renderPanelSheet: false,
    panel: _floatingPanel(),
    body: Center(
      child: Text("This is the Widget behind the sliding panel"),
    ),
  );
}


